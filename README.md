# 2Coders Task Project

This is the solution for the 2Coders Task Project. This app contains 4 major screens:

- Login
- Main
- Movie Details
- Favourite movies

By default the user is navigated to `/movies` route where they are shown a list of popular movies. At the top a search input is present. When users search for a keyword the entire list of movies is updated. When users clear the input the list is reset back to popular movies. At the bottom a pagination component is shown.

When a movie card is hovered over its details are shown (name, overview, popularity and release date). If users press the card they will be navigated to the movie details screen. If users are logged in 2 new components will be visible in this screen - Rate this movie and Add to favourites.

## Notes

### Functional components

All components used in the project are functional ones. Strived to make them as simple as possible. Could probably refactor and split up the bigger ones into smaller ones if I had more time.

### React Router

This router package was used in this project. All routes are defined in `./src/routes.tsx`.

### Some state management logic (contexts, redux, whatever)

Zustand was used to manage the entire frontend state. Its quick, easy to use and works in hooks - perfect for functional components. Also, it offers persistant storage (for storing the favourites and ratings).

### Implement the CSS using SASS, LESS, or Styled components

I've used Tailwind CSS + DaisyUI for this project so there was no real need to use CSS/SASS/LESS or Styled components. I could have gone with a solution that imports CSS/SASS file per component but I thought Tailwind CSS would do the trick just fine in this instance.

### Tests

I used Vitest + React Testing Library to setup tests in this project. Due to time constraints I didn't manage to write a lot of tests. I wrote component and store tests showcasing the use of React Testing Library and mocking Zustand (which is used to store data).

### Error boudnary

Used Error boundary in multiple places to showcase its use.

### React Portals

Didn't see the need to use them in this instance. Added a demo one in the Home page to showcase its use.

### TypeScript

TypeScript was used accross the project.

---

## Setup

To install all project dependencies please run:

```
npm run install
```

## Development

To run the project in development mode:

```
npm run dev
```

## Testing

To run all tests:

```
npm run test
```

This will start Vitest in watch mode. If you prefer its visual UI you can run:

```
npm run testUI
```
