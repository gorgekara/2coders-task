import React from "react";
import { Outlet } from "react-router-dom";
import { Header } from "@components/Header";

export const MovieLayout: React.FunctionComponent = () => {
  return (
    <div className="z-1 relative bg-white min-h-screen">
      <Header />
      <div className="pt-10 container text-black mx-auto max-w-screen-md lg:max-w-screen-lg z-1 relative">
        <Outlet />
      </div>
    </div>
  );
};
