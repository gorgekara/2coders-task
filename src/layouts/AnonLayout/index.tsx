import React from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { useAuthStore } from "@stores/auth";

export const AnonLayout: React.FunctionComponent = () => {
  const token = useAuthStore((store) => store.token);
  const navigate = useNavigate();

  // DO NOT ALLOW LOGGED IN USERS
  // TO SEE THE LOGIN PAGE
  React.useEffect(() => {
    if (token) {
      navigate("/movies");
      return;
    }
  }, [token, navigate]);

  return (
    <div className="">
      <Outlet />
    </div>
  );
};
