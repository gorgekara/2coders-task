import { Header } from "@components/Header";
import { act, render, renderHook, screen } from "@services/test-utils";
import { useAuthStore } from "@stores/auth";

test("Header by default should not have user profile menu", () => {
  render(<Header />);
  expect(screen.queryByTestId("profile-menu")).not.toBeInTheDocument();
});

test("Header should have user profile menu for logged in user", async () => {
  render(<Header />);
  const { result }: any = renderHook(useAuthStore);

  // LOGIN USER WITH AUTH HOOK
  await act(() =>
    result.current.loginUser("random@mail.com", "randomPass123!@#")
  );

  expect(screen.queryByTestId("profile-menu")).toBeInTheDocument();
});
