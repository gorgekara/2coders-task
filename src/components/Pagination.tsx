interface PaginationTypes {
  page: number;
  totalPages: number;
  totalResults: number;
  goToPage: (page: number) => void;
}

export const Pagination = ({
  page,
  totalPages,
  totalResults,
  goToPage,
}: PaginationTypes) => {
  return (
    <nav aria-label="Page navigation">
      <ul className="inline-flex -space-x-px text-sm">
        {page - 1 > 0 ? (
          <li>
            <button
              onClick={() => goToPage(page - 1)}
              className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-e-0 border-gray-300  hover:bg-gray-100 hover:text-gray-700 "
            >
              Previous
            </button>
          </li>
        ) : null}
        {page - 2 > 0 ? (
          <li>
            <button
              onClick={() => goToPage(page - 2)}
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700"
            >
              {page - 2}
            </button>
          </li>
        ) : null}
        {page - 1 > 0 ? (
          <li>
            <button
              onClick={() => goToPage(page - 1)}
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700"
            >
              {page - 1}
            </button>
          </li>
        ) : null}
        <li>
          <button className="flex items-center justify-center px-3 h-8 leading-tight  bg-blue-500 text-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700">
            {page}
          </button>
        </li>
        {totalPages > page + 1 ? (
          <li>
            <button
              onClick={() => goToPage(page + 1)}
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700"
            >
              {page + 1}
            </button>
          </li>
        ) : null}
        {totalPages > page + 2 ? (
          <li>
            <button
              onClick={() => goToPage(page + 2)}
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 "
            >
              {page + 2}
            </button>
          </li>
        ) : null}

        {totalPages > page + 1 ? (
          <li>
            <button
              onClick={() => goToPage(page + 1)}
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 "
            >
              Next
            </button>
          </li>
        ) : null}
      </ul>
      <div className="inline text-sm ml-10">
        <span className="font-semibold">Total results: </span>
        {totalResults}
      </div>
    </nav>
  );
};
