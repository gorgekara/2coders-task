import React from "react";

interface RatingTypes {
  rating?: number;
  onRate: (rating: number) => void;
}

export const Rating = ({ rating = 0, onRate }: RatingTypes) => {
  const [_rating, setRating] = React.useState(0);
  const ratingStars = Array.from(Array(10).keys()); // 10 Stars

  const _onRate = (rating: number) => {
    onRate(rating);
    setRating(rating);
  };

  React.useEffect(() => {
    setRating(rating);
  }, [rating]);

  return (
    <div className="flex flex-col justify-center bg-neutral-100 px-4 py-2">
      <div className="font-bold text-sm">Rate this movie:</div>
      <div className="rating">
        {ratingStars?.map((star) => (
          <input
            defaultChecked={rating - 1 === star}
            key={star}
            type="radio"
            name="rating-2"
            onClick={() => _onRate(star + 1)}
            className={`mask mask-star-2 w-4 ${
              _rating === 0 ? "bg-neutral-200" : "bg-amber-500"
            }`}
          />
        ))}
      </div>
    </div>
  );
};
