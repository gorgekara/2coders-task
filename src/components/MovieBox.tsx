import { populateMovieImage, truncate } from "@services/utils";
import { Link } from "react-router-dom";
import { MovieType } from "src/types";

interface MovieBoxTypes {
  movie: MovieType;
}

export const MovieBox = ({ movie }: MovieBoxTypes) => {
  return (
    <div className="relative group w-1/5 bg-white transition-all transform hover:scale-125 hover:z-50 hover:drop-shadow-lg">
      <Link
        to={`/movies/details/${movie.id}`}
        className="w-full h-80 bg-cover flex flex-col justify-end"
        style={{
          backgroundImage: `url(${populateMovieImage(movie?.poster_path)})`,
        }}
      >
        <div className="flex flex-col justify-center p-5 h-44 opacity-0 bg-white/0 group-hover:opacity-100 group-hover:bg-white/80 transition-all text-xs group-hover:backdrop-filter group-hover:backdrop-blur-md">
          <h5 className="mb-1 text-sm font-bold tracking-tight text-gray-900">
            {movie?.title}
          </h5>
          <div className="flex flex-row mb-2 gap-2 text-[10px]">
            <div>
              <svg
                className="w-3 h-3 text-gray-500 inline align-[-2px] mr-0.5"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 10h16m-8-3V4M7 7V4m10 3V4M5 20h14c.6 0 1-.4 1-1V7c0-.6-.4-1-1-1H5a1 1 0 0 0-1 1v12c0 .6.4 1 1 1Zm3-7h0v0h0v0Zm4 0h0v0h0v0Zm4 0h0v0h0v0Zm-8 4h0v0h0v0Zm4 0h0v0h0v0Zm4 0h0v0h0v0Z"
                />
              </svg>{" "}
              {movie?.release_date}
            </div>
            <div className="text-black/30">|</div>
            <div>
              <svg
                className="w-3 h-3 text-amber-500 inline align-[-2px] mr-0.5"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 24 24"
              >
                <path d="M13.8 4.2a2 2 0 0 0-3.6 0L8.4 8.4l-4.6.3a2 2 0 0 0-1.1 3.5l3.5 3-1 4.4c-.5 1.7 1.4 3 2.9 2.1l3.9-2.3 3.9 2.3c1.5 1 3.4-.4 3-2.1l-1-4.4 3.4-3a2 2 0 0 0-1.1-3.5l-4.6-.3-1.8-4.2Z" />
              </svg>
              {movie?.popularity}
            </div>
          </div>
          <p className="font-normal text-gray-700 ">
            {truncate(movie?.overview, 100)}
          </p>
        </div>
      </Link>
    </div>
  );
};
