import { Link } from "react-router-dom";
import { User } from "src/types";

interface ProfileMenuTypes {
  user: User;
  onLogout: () => void;
}

export const ProfileMenu = ({ user, onLogout }: ProfileMenuTypes) => {
  return (
    <div
      className="dropdown dropdown-end dropdown-hover"
      data-testid="profile-menu"
    >
      <div
        tabIndex={0}
        role="button"
        className="btn m-1 text-neutral-600 bg-neutral-100 hover:bg-neutral-200 border-none rounded-none"
      >
        <svg
          className="w-6 h-6 text-neutral-600"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
        >
          <path
            stroke="currentColor"
            strokeWidth="2"
            d="M7 17v1c0 .6.4 1 1 1h8c.6 0 1-.4 1-1v-1a3 3 0 0 0-3-3h-4a3 3 0 0 0-3 3Zm8-9a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
          />
        </svg>{" "}
        {user?.firstName}{" "}
        <svg
          className="w-2.5 h-2.5 ms-3"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 10 6"
        >
          <path
            stroke="currentColor"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="m1 1 4 4 4-4"
          />
        </svg>
      </div>
      <ul
        tabIndex={0}
        className="dropdown-content z-[1] menu p-2 shadow rounded-none w-52 bg-white text-black"
      >
        <li className="hover:bg-neutral-200 focus:bg-neutral-200 active:bg-neutral-200">
          <Link to="/movies/favourites" className="block px-4 py-2 ">
            My Favourites
          </Link>
        </li>
        <li>
          <a href="#" onClick={onLogout} className="block px-4 py-2">
            Log out
          </a>
        </li>
      </ul>
    </div>
  );
};
