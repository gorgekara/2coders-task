interface FavouriteTypes {
  isFavourite?: boolean;
  onClick: () => void;
}

export const Favourite = ({ isFavourite = false, onClick }: FavouriteTypes) => {
  return (
    <div className="flex flex-col justify-center">
      <button
        onClick={onClick}
        className="px-4 py-5 bg-neutral-100 justify-center flex font-bold text-sm"
      >
        <div
          className={`mt-0.5 inline-block w-4 h-4 mask mask-heart ${
            isFavourite ? "bg-red-500" : "bg-neutral-400"
          } mr-2`}
        ></div>
        {isFavourite ? "Remove Favourite" : "Add to Favourites"}
      </button>
    </div>
  );
};
