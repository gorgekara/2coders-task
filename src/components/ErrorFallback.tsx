export const ErrorFallback = () => {
  return (
    <div className="p-4 bg-red-100 w-full h-20 justify-center flex items-center text-lg">
      Something went wrong. Please reload the page and try again!
    </div>
  );
};
