import { AnonLayout } from "@layouts/AnonLayout";
import { MovieLayout } from "@layouts/MovieLayout";
import { FavouriteMovies } from "@pages/FavouriteMovies";
import { Home } from "@pages/Home";
import { Login } from "@pages/Login";
import { MovieDetails } from "@pages/MovieDetails";
import { NotFound } from "@pages/NotFound";

export const routes = [
  {
    path: "/",
    Component: AnonLayout,
    children: [
      {
        path: "",
        Component: Login,
      },
      {
        path: "login",
        Component: Login,
      },
    ],
  },
  {
    path: "/movies",
    Component: MovieLayout,
    children: [
      {
        path: "",
        Component: Home,
      },
      {
        path: "details/:movieId",
        Component: MovieDetails,
      },
      {
        path: "favourites",
        Component: FavouriteMovies,
      },
      {
        path: "*",
        Component: NotFound,
      },
    ],
  },
  {
    path: "*",
    Component: AnonLayout,
    children: [
      {
        path: "*",
        Component: NotFound,
      },
    ],
  },
];
