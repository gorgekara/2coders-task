import { API } from "@services/api";

export const getPopularMovies = async (page = 1) => {
  const params = new URLSearchParams({
    page: page.toString(),
    language: "en-US",
  });
  return await API.get(`/movie/popular?${params}`);
};

export const getMovies = async (page = 1, query: string) => {
  const params = new URLSearchParams({ query, page: page.toString() });
  return await API.get(`/search/movie?${params}`);
};

export const getMovie = async (movieId: number) => {
  return await API.get(`/movie/${movieId}?language=en-US`);
};
