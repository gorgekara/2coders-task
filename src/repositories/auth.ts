import { API } from "@services/api";

export const loginUser = async (email: string, password: string) => {
  return await API.post("/auth/login", {
    email,
    password,
  });
};
