export const truncate = (strToTruncate: string, limit: number) => {
  return strToTruncate.length > limit
    ? strToTruncate.slice(0, limit - 1) + "..."
    : strToTruncate;
};

export const populateMovieImage = (image: string | undefined) => {
  if (!image?.length) return;
  return `https://image.tmdb.org/t/p/w500/${image}`;
};
