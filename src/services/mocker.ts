export const mockPost: Record<string, any> = {
  "/auth/login": {
    data: {
      user: {
        email: "gorgekara@gmail.com",
        firstName: "Gjorge",
        lastName: "Karakabakov",
      },
      token: "my-very-interesting-jwt-token",
    },
  },
};
