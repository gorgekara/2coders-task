import { mockPost } from "./mocker";

const BASE_URL = import.meta.env.VITE_API_URL;
const API_KEY = import.meta.env.VITE_API_KEY;

export const API = {
  // MOCKING THE POST REQUEST SINCE WE DON'T HAVE
  // A BACKEND ENDPOINT TO AUTH AGAINST
  post: async (location: string, data: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      if (mockPost[location]) {
        console.log(`REQUEST URL: ${location} WITH DATA: ${data}`);
        resolve(mockPost[location]);
      } else {
        reject("URL not found");
      }
    });
  },
  get: async (location: string, options: any = {}) => {
    return await fetch(`${BASE_URL}${location}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${API_KEY}`,
        Accept: "application/json",
      },
      ...options,
    }).then((response) => response.json());
  },
};
