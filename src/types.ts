export interface User {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}

export interface LoginResponse {
  user: User;
  token: string;
}

export interface MovieType {
  id: number;
  title: string;
  poster_path: string;
  popularity: number;
  overview: string;
  release_date: string;
  budget?: number;
  revenue?: number;
  original_title?: string;
}
