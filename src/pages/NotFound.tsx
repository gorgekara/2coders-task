export const NotFound = () => {
  return (
    <section className="flex items-center w-full justify-center h-screen p-16 bg-blue-150 dark:bg-gray-700">
      <div className="flex flex-col gap-6 text-center">
        <h2 className="font-extrabold text-9xl text-gray-600 dark:text-gray-100">
          <span className="sr-only">Error</span>404
        </h2>
        <p className="text-2xl md:text-3xl dark:text-gray-300">
          Sorry, we couldn't find this page.
        </p>
        <a
          href="/"
          className="mt-10 px-8 py-4 text-xl font-semibold rounded bg-blue-600 text-gray-50 hover:text-gray-200"
        >
          Back to home
        </a>
      </div>
    </section>
  );
};
