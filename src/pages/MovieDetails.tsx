import React from "react";
import { Favourite } from "@components/Favourite";
import { Loading } from "@components/Loading";
import { Rating } from "@components/Rating";
import { populateMovieImage } from "@services/utils";
import { useAuthStore } from "@stores/auth";
import { useMovieStore } from "@stores/movies";
import { useProfileStore } from "@stores/profile";
import { useParams } from "react-router-dom";
import { ErrorBoundary } from "react-error-boundary";
import { ErrorFallback } from "@components/ErrorFallback";

export const MovieDetails = () => {
  let { movieId } = useParams();
  const getMovie = useMovieStore((store) => store.getMovie);
  const details = useMovieStore((store) => store.details);
  const loading = useMovieStore((store) => store.loading);
  const favourites = useProfileStore((store) => store.favourites);
  const addFavourite = useProfileStore((store) => store.addFavourite);
  const removeFavourite = useProfileStore((store) => store.removeFavourite);
  const addRating = useProfileStore((store) => store.addRating);
  const ratings = useProfileStore((store) => store.ratings);
  const user = useAuthStore((store) => store.user);

  const fetchMovie = React.useCallback(async () => {
    const movie = parseInt(movieId as string, 10);
    await getMovie(movie);
  }, [movieId]);

  const toggleFavourite = React.useCallback(() => {
    if (!details) return;

    if (favourites.find((fav) => fav.id === details?.id)) {
      return removeFavourite(details?.id);
    }

    addFavourite(details);
  }, [details?.id]);

  const onRate = React.useCallback(
    (rating: number) => {
      if (!details) return;
      addRating(details?.id, rating);
    },
    [details?.id]
  );

  React.useEffect(() => {
    fetchMovie();
  }, []);

  if (loading) {
    return <Loading />;
  }

  return (
    <div className="page">
      <div className="flex flex-row gap-10">
        <img
          className="flex-1"
          src={populateMovieImage(details?.poster_path)}
          alt={details?.title}
        />
        <div className="flex-2">
          <h1 className="font-bold text-3xl">{details?.title}</h1>
          <div className="flex flex-row mt-3 gap-2 text-sm">
            <div>
              <svg
                className="w-4 h-4 text-gray-500 inline align-[-2px] mr-0.5"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 10h16m-8-3V4M7 7V4m10 3V4M5 20h14c.6 0 1-.4 1-1V7c0-.6-.4-1-1-1H5a1 1 0 0 0-1 1v12c0 .6.4 1 1 1Zm3-7h0v0h0v0Zm4 0h0v0h0v0Zm4 0h0v0h0v0Zm-8 4h0v0h0v0Zm4 0h0v0h0v0Zm4 0h0v0h0v0Z"
                />
              </svg>{" "}
              {details?.release_date}
            </div>
            <div className="text-black/30">|</div>
            <div>
              <svg
                className="w-4 h-4 text-amber-500 inline align-[-2px] mr-0.5"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 24 24"
              >
                <path d="M13.8 4.2a2 2 0 0 0-3.6 0L8.4 8.4l-4.6.3a2 2 0 0 0-1.1 3.5l3.5 3-1 4.4c-.5 1.7 1.4 3 2.9 2.1l3.9-2.3 3.9 2.3c1.5 1 3.4-.4 3-2.1l-1-4.4 3.4-3a2 2 0 0 0-1.1-3.5l-4.6-.3-1.8-4.2Z" />
              </svg>
              {details?.popularity}
            </div>
          </div>
          <p className="mt-5">{details?.overview}</p>
          <ul className="mt-10 text-sm">
            <li className="flex flex-row mb-4">
              <div className="font-bold w-40">Budget</div>
              <div>${details?.budget}</div>
            </li>
            <li className="flex flex-row mb-4">
              <div className="font-bold w-40">Revenue</div>
              <div>${details?.revenue}</div>
            </li>
            <li className="flex flex-row mb-4">
              <div className="font-bold w-40">Original Title</div>
              <div>{details?.original_title}</div>
            </li>
            <li className="flex flex-row mb-4">
              <div className="font-bold w-40">Release Date</div>
              <div>{details?.release_date}</div>
            </li>
          </ul>
          <ErrorBoundary fallback={<ErrorFallback />}>
            {user ? (
              <div className="flex flex-row gap-3 mt-5 pt-5 border-t border-neutral-200">
                {details ? (
                  <Rating rating={ratings?.[details?.id]} onRate={onRate} />
                ) : null}
                <Favourite
                  isFavourite={
                    !!favourites.find((fav) => fav.id === details?.id)
                  }
                  onClick={toggleFavourite}
                />
              </div>
            ) : null}
          </ErrorBoundary>
        </div>
      </div>
    </div>
  );
};
