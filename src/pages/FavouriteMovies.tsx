import { MovieBox } from "@components/MovieBox";
import { useAuthStore } from "@stores/auth";
import { useProfileStore } from "@stores/profile";
import React from "react";
import { useNavigate } from "react-router-dom";

export const FavouriteMovies = () => {
  const favourites = useProfileStore((store) => store.favourites);
  const user = useAuthStore((store) => store.user);
  const navigate = useNavigate();

  React.useEffect(() => {
    if (!user) {
      navigate("/movies", { replace: true });
      return;
    }
  }, [user]);

  if (!user) return null;

  return (
    <div className="page">
      <h1 className="text-2xl font-bold mb-5">My Favourites</h1>
      <div className="flex flex-wrap">
        {!favourites?.length ? (
          <div className="bg-neutral-50 h-44 w-full flex justify-center items-center">
            You don't have any favourites yet. Use the{" "}
            <span className=" bg-neutral-200 px-3 py-4 ml-2 mr-2 font-bold text-sm justify-center flex">
              <span className="mask mask-heart w-4 h-4 inline-block bg-red-600 mr-1 mt-0.5"></span>{" "}
              Add to favourites
            </span>{" "}
            button to add movies here!
          </div>
        ) : (
          favourites?.map((movie) => <MovieBox key={movie.id} movie={movie} />)
        )}
      </div>
    </div>
  );
};
