import { LoginForm } from "@components/LoginForm";
import { useAuthStore } from "@stores/auth";
import { useNavigate } from "react-router-dom";

export const Login = () => {
  const navigate = useNavigate();
  const loginUser = useAuthStore((store) => store.loginUser);

  const onSubmit = async (email: string, password: string) => {
    const data = await loginUser(email, password);
    if (data?.user) {
      navigate("/movies");
    }
  };

  return <LoginForm onSubmit={onSubmit} />;
};
