import React from "react";
import { createPortal } from "react-dom";
import { MovieBox } from "@components/MovieBox";
import { Pagination } from "@components/Pagination";
import { Search } from "@components/Search";
import { useMovieStore } from "@stores/movies";
import { Loading } from "@components/Loading";
import { ErrorBoundary } from "react-error-boundary";
import { ErrorFallback } from "@components/ErrorFallback";

export const Home = () => {
  const getPopularMovies = useMovieStore((store) => store.getPopularMovies);
  const results = useMovieStore((store) => store.results);
  const page = useMovieStore((store) => store.page);
  const totalPages = useMovieStore((store) => store.totalPages);
  const totalResults = useMovieStore((store) => store.totalResults);
  const getMovies = useMovieStore((store) => store.getMovies);
  const query = useMovieStore((store) => store.query);
  const loading = useMovieStore((store) => store.loading);

  const fetchPopularMovies = async (page = 1) => {
    if (query) {
      await searchMovies(page, query);
    } else {
      await getPopularMovies(page);
    }
  };

  const searchMovies = async (page = 1, _query = "") => {
    await getMovies(page, _query);
  };

  const onSearch = async (_query: string) => {
    if (_query?.length) {
      await searchMovies(1, _query);
    } else {
      await getPopularMovies(1);
    }
  };

  React.useEffect(() => {
    fetchPopularMovies();
  }, []);

  return (
    <div className="page">
      <Search onSubmit={onSearch} />
      <div className="flex flex-wrap">
        <ErrorBoundary fallback={<ErrorFallback />}>
          {loading ? (
            <Loading />
          ) : (
            results?.map((movie) => <MovieBox movie={movie} key={movie.id} />)
          )}
        </ErrorBoundary>

        <div className="pt-5 pb-10">
          <Pagination
            page={page}
            goToPage={fetchPopularMovies}
            totalPages={totalPages}
            totalResults={totalResults}
          />
        </div>
      </div>
      {createPortal(
        <div className="bg-red-100 text-sm font-bold p-4 w-96 mx-auto">
          This is just an example of a React Portal - Didn't know where to use
          it in ths project 🤷
        </div>,
        document.body
      )}
    </div>
  );
};
