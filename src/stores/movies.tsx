import { getMovie, getMovies, getPopularMovies } from "@repositories/movies";
import { MovieType } from "src/types";
import { create } from "zustand";

interface MovieStore {
  results: MovieType[];
  page: number;
  totalPages: number;
  totalResults: number;
  query: string | undefined;
  loading: boolean;
  details: MovieType | undefined;

  getPopularMovies: (page?: number) => void;
  getMovies: (page?: number, query?: string) => void;
  getMovie: (movieId: number) => void;
}

export const useMovieStore = create<MovieStore>()((set) => ({
  page: 0,
  totalPages: 0,
  totalResults: 0,
  query: undefined,
  results: [],
  loading: false,
  details: undefined,

  getPopularMovies: async (page = 1) => {
    set({ loading: true, query: undefined });

    const response: any = await getPopularMovies(page);
    set({
      loading: false,
      results: response?.results,
      page: response?.page,
      totalPages: response?.total_pages,
      totalResults: response?.total_results,
    });
  },

  getMovies: async (page = 1, query = "") => {
    set({
      loading: true,
      results: [],
      page,
      totalPages: 0,
      totalResults: 0,
      query,
    });

    const response = await getMovies(page, query);
    set({
      loading: false,
      results: response?.results,
      page: response?.page,
      totalPages: response?.total_pages,
      totalResults: response?.total_results,
    });
  },

  getMovie: async (movieId: number) => {
    set({ loading: true, details: undefined });
    const response = await getMovie(movieId);
    set({ loading: false, details: response });
  },
}));
