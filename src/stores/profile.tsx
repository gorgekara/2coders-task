import { MovieType } from "src/types";
import { create } from "zustand";
import { persist } from "zustand/middleware";

interface AuthState {
  favourites: Array<MovieType>;
  ratings: Record<string, number>;

  addFavourite: (movie: MovieType) => void;
  removeFavourite: (id: number) => void;
  addRating: (id: number, rating: number) => void;
}

export const useProfileStore = create<AuthState>()(
  persist(
    (set, get) => ({
      favourites: [],
      ratings: {},

      addRating: async (id, rating) => {
        const temp = get().ratings;
        temp[id] = rating;
        set({ ratings: temp });
      },

      addFavourite: async (movie) => {
        const alreadyIn = get().favourites?.find(
          (favourite) => movie.id === favourite.id
        );

        if (!alreadyIn) {
          set({
            favourites: [...get().favourites, movie],
          });
        }
      },

      removeFavourite: async (id) => {
        set({
          favourites: get().favourites.filter(
            (favourite) => favourite.id !== id
          ),
        });
      },
    }),
    {
      name: "profile",
    }
  )
);
