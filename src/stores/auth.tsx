import { loginUser } from "@repositories/auth";
import { User } from "src/types";
import { create } from "zustand";
import { persist } from "zustand/middleware";

interface AuthState {
  user: User | null;
  token: string | null;

  loginUser: (email: string, password: string) => Promise<any>;
  logoutUser: () => void;
}

export const useAuthStore = create<AuthState>()(
  persist(
    (set) => ({
      user: null,
      token: null,

      loginUser: async (email: string, password: string) => {
        try {
          const response: any = await loginUser(email, password);
          const data = response?.data;
          set({
            user: data.user,
            token: data.token,
          });
          return data;
        } catch (error) {
          console.log(error);
        }
      },

      logoutUser: async () => {
        try {
          set({
            token: null,
            user: null,
          });
        } catch (error) {
          console.log(error);
        }
      },
    }),
    {
      name: "auth",
    }
  )
);
