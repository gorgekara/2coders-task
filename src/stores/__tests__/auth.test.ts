import { useAuthStore } from "@stores/auth";
import { act, renderHook } from "@services/test-utils";

// AN EXAMPLE TEST ON HOW ZUSTAND STORES
// CAN BE TESTED - MULTIPLE TESTS CAN BE ADDED HERE TO IMPROVE
// OVERALL CODE COVERAGE
describe("Stores > Auth", () => {
  test("useAuthStore initial state should be clean", () => {
    const { result }: any = renderHook(useAuthStore);
    expect(result.current?.user).toBe(null);
    expect(result.current?.token).toBe(null);
  });

  test("loginUser should return mocked user data", async () => {
    const { result }: any = renderHook(useAuthStore);

    const response = await act(() =>
      result.current.loginUser("random@mail.com", "randomPass123!@#")
    );
    expect(response?.user).toBeDefined();
  });

  test("logged in user should be logged out properly", async () => {
    const { result }: any = renderHook(useAuthStore);

    await act(() =>
      result.current.loginUser("random@mail.com", "randomPass123!@#")
    );
    expect(result.current?.user?.firstName).toBeDefined();
    expect(result.current?.token).toBeDefined();

    await act(() => result.current.logoutUser());

    expect(result.current?.user?.firstName).toBe(undefined);
    expect(result.current?.token).toBe(null);
  });
});
